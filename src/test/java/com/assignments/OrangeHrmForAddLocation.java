package com.assignments;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class OrangeHrmForAddLocation {
	
	public static void main(String[] args) throws InterruptedException {

		WebDriver driver = new ChromeDriver();

		driver.get("https://opensource-demo.orangehrmlive.com");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		Thread.sleep(2000);
		driver.findElement(By.xpath("//input[@name='username']")).sendKeys("Admin");
		driver.findElement(By.xpath("//input[@name='password']")).sendKeys("admin123");
		driver.findElement(By.xpath("//button[@type='submit']")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[text()='Admin']")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//span[text()='Organization ']")).click();
		driver.findElement(By.partialLinkText("Locations")).click();

		driver.findElement(By.xpath("//button[text()=' Add ']")).click();
		driver.findElement(By.xpath("(//input[@placeholder='Type here ...'])[1]")).sendKeys("QaAutomationOffice1");
		driver.findElement(By.xpath("(//input[@placeholder='Type here ...'])[2]")).sendKeys("Renoldsbug");
		driver.findElement(By.xpath("(//input[@placeholder='Type here ...'])[3]")).sendKeys("ohio");
		driver.findElement(By.xpath("(//input[@placeholder='Type here ...'])[4]")).sendKeys("43068");
		driver.findElement(By.xpath("//i[@class='oxd-icon bi-caret-down-fill oxd-select-text--arrow']")).click();
		driver.findElement(By.xpath("//*[contains(text(),'United States')]")).click();

		driver.findElement(By.xpath("(//input[@placeholder='Type here ...'])[5]")).sendKeys("999999999");
		driver.findElement(By.xpath("(//input[@placeholder='Type here ...'])[6]")).sendKeys("425001");
		driver.findElement(By.xpath("(//textarea[@placeholder='Type here ...'])[1]"))
				.sendKeys("2793 Taylor Rd Ext, Reynoldsburg, OH 43068, United States");
		driver.findElement(By.xpath("(//textarea[@placeholder='Type here ...'])[2]")).sendKeys("hello");
		Thread.sleep(2000);
		driver.findElement(By.xpath("//button[@type='submit']")).click();
		String text = driver.findElement(By.xpath("//div[text()='QaAutomationOffice']")).getText();
		System.out.println(text);
		driver.findElement(By.xpath("//i[@class=\"oxd-icon bi-caret-down-fill oxd-userdropdown-icon\"]")).click();
		driver.findElement(By.xpath("//a[text()='Logout']")).click();
		driver.close();
	}

}

